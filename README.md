# ArchOrange

This is a set of scripts an guides to setup Arch with my configurations.

## archinstall

Boot an Arch installation medium and perform the following actions:

During the archinstall process you should configure a user profile.

```
cd
curl -sSLO https://gitlab.com/azureorange404/archorange/-/raw/main/archinstall/user_configuration.json
curl -sSLO https://gitlab.com/azureorange404/archorange/-/raw/main/archinstall/user_disk_layout.json

archinstall --config user_configuration.json --disk-layout user_disk_layout.json

reboot
```

## setup

While using the setup script, you will be prompted for y/n or sudo password multiple times.

Do not run this script as root, as this will not be necessary (sudo will be used) or maybe even won't work at some parts of the script.

```
cd
git clone https://gitlab.com/azureorange404/archorange.git
cd archorange
bash setup.sh
```
