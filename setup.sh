#!/bin/bash

echo -ne "
                                █                           
                    ███         █   ███                     
                    █ █ ███ ███ ███ █ █ ███ ███ ███ ███ ███ 
                    █ █ █   █   █ █ █ █ █     █ █ █ █ █ ███ 
                    ███ █   █   █ █ █ █ █   ███ █ █ █ █ █   
                    █ █ █   ███ █ █ ███ █   ███ █ █ ███ ███ 
                                                      █     
                                                    ███     
"

if [[ $EUID -eq 0 ]]; then
   echo "This script must NOT be run as root." 
   exit 1
fi

# variables needed in this an following scripts

DIR="$(cd "$(dirname "$0")" && pwd)"

# functions repeatedly used by this script

yesno() {
while true; do
    read -p "$1 (Y/n)" yn
    case $yn in
        [Yy]* ) "$2"; break;;
        [Nn]* ) echo "This step was skipped."; break;;
        * )     "$2"; break;;
    esac
done
}

echo -ne "
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄

"
echo -ne "
------------------------------------------------------------------------------------
									  AUR
------------------------------------------------------------------------------------
"

setup_aur() {
    cd $DIR
    bash pkgs/aur_setup.sh
    source $DIR/variables/aur_helper.sh
}

yesno "Do you want to use the AUR?" setup_aur

echo -ne "
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄

"
echo -ne "
------------------------------------------------------------------------------------
						            timeshift
------------------------------------------------------------------------------------
"

backtostart() {
    bash $DIR/setup.sh && exit 1
}

create_snapshot() {
    sudo timeshift --create --btrfs --comments "initial snapshot"
}

install_timeshift() {
    if [[ $(type -t aur_helper) == function ]]; then
        aur_helper timeshift
        yesno "Create a btrfs snapshot now?" create_snapshot
    else
        echo "You need an AUR helper to be installed in order to get timeshift."
		yesno "Do you want to install an AUR helper?" backtostart
    fi
}

yesno "Do you want to use timeshift for creating system snapshots?" install_timeshift

echo -ne "
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄

"
echo -ne "
------------------------------------------------------------------------------------
						            dotfiles
------------------------------------------------------------------------------------
"
setup_config() {
	mkdir $HOME/git
	cd $HOME/git
    git clone https://gitlab.com/azureorange404/dotfiles.git
    cp -r $HOME/git/dotfiles/. $HOME/
    git clone https://gitlab.com/azureorange404/shell-scripts.git
    mkdir -p $HOME/.config/shell/scripts
	cp -r $HOME/git/shell-scripts/. $HOME/.config/shell/scripts/
	
	# add this to a vim dialog script
    mkdir -p ~/.vim/autoload ~/.vim/bundle
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
	
	cd $HOME/.vim/bundle
	git clone https://github.com/romgrk/doom-one.vim.git
	git clone https://github.com/hrsh7th/cmp-path.git
	git clone https://github.com/glepnir/dashboard-nvim.git
	git clone https://github.com/donRaphaco/neotex.git
    git clone https://github.com/hrsh7th/nvim-cmp.git
	git clone https://github.com/vim-airline/vim-airline-themes.git
	git clone https://github.com/tpope/vim-commentary.git
	git clone https://github.com/ap/vim-css-color.git
    cd $DIR
}

setup_de_CH-latin1() {
    setxkbmap -model acer_laptop -layout ch
}
yesno "Do you want me to grab azureorange404's config files?" setup_config
yesno "Do you want to switch X11 keyboardlayout to de_CH-latin1?" setup_de_CH-latin1

echo -ne "
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄

"
echo -ne "
------------------------------------------------------------------------------------
									packages
------------------------------------------------------------------------------------
"

install_pkgs() {
    cd $DIR
    bash pkgs/pkg_install.sh
}

yesno "Do you want to start the package installation dialog?" install_pkgs

echo -ne "
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄

"
echo -ne "
------------------------------------------------------------------------------------
                                      SDDM
------------------------------------------------------------------------------------
"

setup_sddm() {
    # sudo pacman -Sy sddm
    sudo systemctl enable sddm
    
	touch sddm.conf
    echo "[Theme]" >> sddm.conf
    echo "Current=ArchOrange" >> sddm.conf
    sudo mv sddm.conf /etc/sddm.conf
}

# Sddm does not seem to start xmonad ... using startx instead
# yesno "Do you want me to set up sddm for you?" setup_sddm

echo -ne "
------------------------------------------------------------------------------------
								     Xmonad
------------------------------------------------------------------------------------
"

setup_xmonad() {
	cp $DIR/scripts/.profile $HOME/
	bash &

    curl -sSL https://get.haskellstack.org/ | sh
    stack setup

	mkdir -p $HOME/.xmonad
    cd $HOME/.xmonad

    git clone "https://github.com/xmonad/xmonad" xmonad-git
    git clone "https://github.com/xmonad/xmonad-contrib" xmonad-contrib-git
    git clone "https://codeberg.org/xmobar/xmobar" xmobar-git

	stack init
	cp -f $DIR/scripts/xmonad_stack.yaml ./stack.yaml
	sudo mkdir /usr/share/xsessions
	sudo cp $DIR/scripts/xmonad.desktop /usr/share/xsessions/

	stack install

	cp -f $DIR/scripts/xmonad_build ./build
	chmod a+x build

	xmonad --recompile
    
    sed '/twm\ &/d' /etc/X11/xinit/xinitrc | \
    sed '/xclock\ -geometry\ 50x50-1+1\ &/d' | \
    sed '/xterm\ -geometry\ 80x50+494+51\ &/d' | \
    sed '/xterm\ -geometry\ 80x20+494-0\ &/d' | \
    sed '/exec\ xterm\ -geometry\ 80x66+0+0\ -name\ login/d' \
    >> ~/.xinitrc

	echo "exec xmonad" >> ~/.xinitrc

    echo -ne "
if [[ -z \$DISPLAY ]] && [[ \$(tty) = /dev/tty1 ]]; then
    startx
fi
" >> $HOME/.bash_profile
}

yesno "Do you want me to set up xmonad for you?" setup_xmonad

echo -ne "
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄

"
echo -ne "
------------------------------------------------------------------------------------
							          done
------------------------------------------------------------------------------------
"

performreboot() {
    sudo reboot
}

yesno "Reeboot now?" performreboot

exit 1
