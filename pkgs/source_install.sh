#!bin/bash

mkdir -p $HOME/.local/bin/src
SRC_DIR="$HOME/.local/bin/src"

echo -ne "
------------------------------------------------------------------------------------
                   installing dependencies for building packages
------------------------------------------------------------------------------------
"

sudo pacman -S --needed base-devel
sudo pacman -S --needed cairo fontconfig libev libjpeg-turbo libxinerama libxkbcommon-x11 libxrandr pam xcb-util-image xcb-util-xrm

echo -ne "
------------------------------------------------------------------------------------
                    Attempting to build packages from source ...
------------------------------------------------------------------------------------
"

cmd=(dialog --separate-output --checklist "Select packages to build from source:" 22 76 16)
options=(1 "dmenu" on
		 2 "dmenu-scripts" on
		 3 "passmenu-otp" on
		 4 "i3lock-color" on
 )
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
clear
for choice in $choices
do
    case $choice in
		1)
		  cd $SRC_DIR
		  git clone https://gitlab.com/azureorange404/dmenu.git
          cd dmenu
		  sudo make clean install
		  ;;
		2)
		  cd $SRC_DIR
		  git clone https://gitlab.com/azureorange404/dmenu-scripts.git
          cd dmenu-scripts
		  chmod u+x ./*
		  ;;
		3)
		  cd $SRC_DIR
		  git clone https://gitlab.com/azureorange404/passmenu-otp.git
		  cd passmenu-otp
          chmod u+x passmenu-otp passmenu.original
		  cp passmenu-otp $HOME/.local/bin/
		  cp passmenu.original $HOME/.local/bin/passmenu
		  ;;
		4)
		  cd $SRC_DIR
		  git clone https://github.com/Raymo111/i3lock-color.git
          cd i3lock-color
		  ./build.sh
		  ./install-i3lock-color.sh
		  ;;
    esac
done
