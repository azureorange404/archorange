#!bin/bash

mkdir -p $HOME/.local/bin/src
SRC_DIR="$HOME/.local/bin/src"

DIR="$(cd "$(dirname "$0")" && cd .. && pwd)"

echo -ne "
------------------------------------------------------------------------------------
                   installing dependencies for building packages
------------------------------------------------------------------------------------
"

sudo pacman -S --needed base-devel
sudo pacman -S --needed cairo fontconfig libev libjpeg-turbo libxinerama libxkbcommon-x11 libxrandr pam xcb-util-image xcb-util-xrm

echo -ne "
------------------------------------------------------------------------------------
                    Attempting to build packages from source ...
------------------------------------------------------------------------------------
"

cmd=(dialog --separate-output --checklist "What AUR helper do you want to use? (Choose only one)" 22 76 16)
options=(1 "paru" on
         2 "yay" off
 )
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
clear
for choice in $choices
do
    case $choice in
		1)  
		  cd $SRC_DIR
          git clone https://aur.archlinux.org/paru.git
          cd paru
          makepkg -si
          export _AUR_HELPER="PARU"
		  mkdir $DIR/variables
		  echo -ne "
#!/bin/bash
export _AUR_HELPER=\"PARU\"
aur_helper() {
    paru -S \$@
}
		  " >> $DIR/variables/aur_helper.sh
		  paru --gendb
		  ;;
		2)
		  cd $SRC_DIR
		  git clone https://aur.archlinux.org/yay.git
		  cd yay
		  makepkg -si
		  if [ $_AUR_HELPER != "PARU" ]
		  then
		    export _AUR_HELPER="YAY"
			mkdir $DIR/variables
		    echo -ne "
#!/bin/bash
export _AUR_HELPER=\"YAY\"
aur_helper() {
    yay -S \$@
}
		    " >> $DIR/variables/aur_helper.sh
	      else
		    echo "Paru is set as AUR helper while using this script"
		  fi
		  yay -Y --gendb
		  yay -Syu --devel
		  yay -Y --devel --save
		  ;;
    esac
done
