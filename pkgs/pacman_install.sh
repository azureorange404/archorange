#!/bin/bash

# Installing pacman packages

declare -a pacman_pkgs

# browsers

cmd=(dialog --separate-output --checklist "Select packages to install through pacman:" 22 76 16)
options=(1 "qutebrowser (web browser)" on    # any option can be set to default to "on"
         2 "firefox" off
		 3 "opera" off
         4 "torbrowser" off
		 5 "pcmanfm (file manager)" on
		 6 "thunar" off
		 7 "ranger" off
		 8 "kitty (terminal emulator)" on
		 9 "alacritty" off 
		 10 "neovim" on
		 11 "dunst" on
		 12 "libreoffice-fresh (enhanced branch)" on
		 13 "libreoffice-still (maintenance branch)" off
		 14 "thunderbird" on
		 15 "gimp" on
		 16 "inkscape" off
		 17 "audacity" on
		 18 "ardour" off
		 19 "feh" on
		 20 "flameshot" on
		 21 "keepassxc" on
		 22 "vlc" on
		 23 "mpv" on
		 24 "zathura (PDF reader)" on
		 25 "texlive-most" off
		 26 "trayer" on
		 27 "xautolock" on
		 28 "yad" on
		 29 "discord" off
		 30 "nextcloud-client" off
		 31 "sxiv" on
		 32 "udiskie" on
		 33 "blueman" on
		 34 "starship" on
		 35 "man-db" on
		 36 "xorg-xmessage" off
		 37 "xorg-xev" on
		 38 "pass-otp" on
		 39 "neofetch" off
		 40 "lolcat" off
		 41 "polkit" on
		 42 "xorg-xsetroot" on
 )
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
clear
for choice in $choices
do
    case $choice in
		1)  pacman_pkgs+=("qutebrowser");;
		2)  pacman_pkgs+=("firefox");;
		3)  pacman_pkgs+=("opera");;
		4)  pacman_pkgs+=("torbrowser-launcher");;
		5)  pacman_pkgs+=("pcmanfm");;
		6)  pacman_pkgs+=("thunar");;
		7)  pacman_pkgs+=("ranger");;
		8)  pacman_pkgs+=("kitty");;
		9)  pacman_pkgs+=("alacritty");;
		10)  pacman_pkgs+=("neovim");;
		11) pacman_pkgs+=("dunst");;
		12)  pacman_pkgs+=("libreoffice-fresh");;
		13) pacman_pkgs+=("libreoffice-still");;
		14) pacman_pkgs+=("thunderbird");;
		15) pacman_pkgs+=("gimp");;
		16) pacman_pkgs+=("inkscape");;
		17) pacman_pkgs+=("audacity");;
		18) pacman_pkgs+=("ardour");;
		19) pacman_pkgs+=("feh");;
		20) pacman_pkgs+=("flameshot");;
		21) pacman_pkgs+=("keepassxc");;
		22) pacman_pkgs+=("vlc");;
		23) pacman_pkgs+=("mpv");;
		24) pacman_pkgs+=("zathura");;
		25) pacman_pkgs+=("texlive-most");;
		26) pacman_pkgs+=("trayer");;
		27) pacman_pkgs+=("xautolock");;
		28) pacman_pkgs+=("yad");;
		29) pacman_pkgs+=("discord");;
		30) pacman_pkgs+=("nextcloud-client");;
		31) pacman_pkgs+=("sxiv");;
		32) pacman_pkgs+=("udiskie");;
		33) pacman_pkgs+=("blueman");;
		34) pacman_pkgs+=("starship");;
		35) pacman_pkgs+=("man-db");;
		36) pacman_pkgs+=("xorg-xmessage");;
		37) pacman_pkgs+=("xorg-xev");;
		38) pacman_pkgs+=("pass-otp");;
		39) pacman_pkgs+=("neofetch");;
		40) pacman_pkgs+=("lolcat");;
		41) pacman_pkgs+=("polkit");;
		42) pacman_pkgs+=("xorg-xsetroot");;
    esac
done

echo ${pacman_pkgs[@]}

while true; do
    read -p "Do you want to install these packages? (Y/n/quit) " yn
    case $yn in
        [Yy]* ) sudo pacman -Sy ${pacman_pkgs[@]}; break;;
		[Nn]* ) source $(realpath "$0"); exit;;
		[Qq]* ) exit;;
        * )     sudo pacman -Sy ${pacman_pkgs[@]}; break;;
    esac
done


