#!/bin/bash

DIR="$(cd "$(dirname "$0")" && cd .. && pwd)"

if test -f "$DIR/variables/aur_helper.sh"; then
    echo "Loading AUR helper variable from backup file ..."
	source $DIR/variables/aur_helper.sh
else
    echo "there seems no AUR helper has been installed with this script."
fi

declare -a aur_pkgs

cmd=(dialog --separate-output --checklist "Select packages to install through pacman:" 22 76 16)
options=(1 "betterlockscreen" on
		 2 "brave" on
		 3 "librewolf" off
		 4 "badwolf" off
		 5 "spotify" off
		 6 "MS teams" off
		 7 "whatsapp" off
		 8 "joplin" off
 )
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
clear
for choice in $choices
do
    case $choice in
		1)  aur_pkgs+=("betterlockscreen");;
		2)  aur_pkgs+=("brave-bin");;
		3)  aur_pkgs+=("librewolf-bin");;
		4)  aur_pkgs+=("badwolf");;
		5)  aur_pkgs+=("spotify");;
		6)  aur_pkgs+=("teams");;
		7)  aur_pkgs+=("whatsapp-for-linux");;
		8)  aur_pkgs+=("joplin-desktop");;
    esac
done

echo ${aur_pkgs[@]}

while true; do
    read -p "Do you want to install these packages? (Y/n/quit) " yn
    case $yn in
        [Yy]* ) aur_helper ${aur_pkgs[@]}; break;;
		[Nn]* ) source $(realpath "$0"); exit;;
		[Qq]* ) exit;;
        * )     aur_helper ${aur_pkgs[@]}; break;;
    esac
done
